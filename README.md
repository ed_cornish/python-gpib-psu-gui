# README #

### What is this repository for? ###

* This is a little utility I wrote to control a Keithley bench PSU via GPIB from a remote session
* I am often working remotely from the lab and sometimes a hard reset of the target device is required :)
* This is intended to save having to physically reset\adjust the PSU or open a test tree just to run the reset node

### NOTES ###

* See the comments in the script for full details, suffice it to say, the GPIB address and commands are **HARD-CODED**
* Feel free to customise them for your own use (the script will print the available GPIB devices it discovers when it starts up)
* There is a dependency on the PyVisa library as well as the NI backend for GPIB control.
    * Full instructions for installing PyVisa and configuring NI-backend are at the [PyVisa homepage](http://pyvisa.readthedocs.org/en/stable/index.html)