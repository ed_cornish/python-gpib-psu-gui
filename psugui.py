from tkinter import *
import time
import visa	#Requires NI backend to be configured and PyVISA module to be installed...

#---------------------------------------------------------------
#	NOTE: This script is intended to run on a windows machine to control a Keithley GPIB enabled power supply from a remote desktop session.
#		  It should be customised appropriately for equipment with different GPIB addresses (visible in the console when the utility starts)
#---------------------------------------------------------------

root = Tk()

root.title("PSU GUI - v0.2 by Ed Cornish")

res_man = visa.ResourceManager()
print (res_man.list_resources())
psu = res_man.open_resource("GPIB::5::INSTR")

print (psu.query("*IDN?"))

voltage = DoubleVar()
current = DoubleVar()

#Set to average readings returned from PSU to smooth the values
averages = 5

v = StringVar()
i = StringVar()
t = StringVar()

p = 0

# Constants for limiting parameters
VOLTAGE_MAX = 4.2
VOLTAGE_MIN = 3.2
CURRENT_MAX = 4.0

def callback():
    print ("clicked!")
    v.set(psu.ask("VOLT?"))

def get_volt():
    v.set(float(psu.ask("VOLT?")))  #Ask for PSU voltage and set v

def set_volt():
    print ("Setting Voltage to:  " + str(voltage.get()))    #console output
    if (VOLTAGE_MIN <= voltage.get() <= VOLTAGE_MAX):                       #check valid range
        psu.write("VOLT " + str(voltage.get()))             #set the PSU to voltage
    else:
        print ("bad voltage!")                #Dangerous voltage specified
    v.set(str(voltage.get()))               #Update voltage monitor with set voltage (need to prune, as could potentially be misleading if not immediately overwritten)

def set_curr():
    print ("Setting Current Limit to: " + str(current.get()))   #console output
    if (current.get() <= CURRENT_MAX):                                   #Safe current limit?
        psu.write("CURR " + str(current.get()))                 #Set to PSU
    else:
        print ("bad current!")                                    #Too high a limit, things could catch on fire...
    
def get_curr():
    i.set(psu.ask("CURR?"))

def get_idraw():
    psu.write("SENS:FUNC 'CURR'")           #Set PSU to measure current drawn
    psu.write("SENS:AVER " + str(averages)) #Set measurement to average over 5 (hard coded at present)
    i.set(float(psu.ask("READ?")))          #Measure and update

def get_outp():
    p = psu.ask("OUTP?")                        #Is the PSU on or off?
    if (int(p) == 1):                           #It's on, green light
        ind.itemconfigure("onoff", fill="green")
    if (int(p) == 0):                           #It's off, red light
        ind.itemconfigure("onoff", fill="red")   
    

def pwr_on():
        psu.write("OUTP 1") #Turn it on
        print ("Turning on")  #Console o/p
def pwr_off():
        psu.write("OUTP 0") #Turn it off
        print ("Turning off") #Console o/p
    

def windexit():     #callback to kill the program
    print ("Quit!")   #Console output
    root.quit()       #Close the window

def task():                                             #main callback task for loop
    timestamp = time.strftime("%a, %b %d, %H:%M:%S")    #get a timestamp - not used for anything except to display current time
    t.set(timestamp)                                    #display the time
    if len(v_entry.get()):                              #retrieve voltage entered by user
        voltage.set(float(v_entry.get()))
    if len(i_entry.get()):                              #retrieve current limit entered by user
        current.set(float(i_entry.get()))
    get_outp()                                          #get PSU state
    get_volt()                                          #get the PSU voltage
    get_idraw()                                         #get the current drawn
    root.update()                                       #update widgets
    root.after(500, task)                               #callback to main task after 100ms

readout_master = Frame(root, width=100, height=50, relief=SUNKEN)
readout_master.pack(side=LEFT)  #wrap readouts in a frame, make them children to it

#Create Voltage readout
v_readout_label = Label(readout_master, anchor=NW, justify=LEFT, text="Voltage Setting:")
v_readout_label.pack()
v_readout = Label(readout_master, anchor=NW, justify=LEFT, textvariable=v)
v_readout.pack()

#Create Current readout
i_readout_label = Label(readout_master, anchor=NW, justify=LEFT, text="Current Drawn :")
i_readout_label.pack()
i_readout = Label(readout_master, anchor=W, justify=LEFT, textvariable=i)
i_readout.pack()

#Create time readout
t_readout = Label(readout_master, anchor=SW, justify=LEFT, textvariable=t)
t_readout.pack()

#Create button to set the voltage
set_volt = Button(root, text="Set V", command=set_volt)
set_volt.pack()

#Create button to set the current
set_curr = Button(root, text="Set I Limit", command=set_curr)
set_curr.pack()

#Create On\Off buttons
on = Button(root, text="PWR ON", command=pwr_on)
on.pack()
off = Button(root, text="PWR OFF", command=pwr_off)
off.pack()

#Create the power status indicator
ind = Canvas(root, width=10, height=10)
ind.pack()
ind.create_rectangle(0,0,10,10, tag="onoff")

#Label the parameter entry fields
v_entry_label = Label(root, text="Voltage Setting:")
v_entry_label.pack()
v_entry = Entry(root)   #entry boxes for current and voltage
v_entry.pack()
i_entry_label = Label(root, text="Current Limit Setting:")
i_entry_label.pack()
i_entry = Entry(root)
i_entry.pack()

#Create a button to exit the utility
ender = Button(root, text="QUIT", command=windexit)
ender.pack()                #button to exit the GUI and close down

v.set("PWR ON")
i.set("PWR ON")

v_entry.insert(0,3.7)
i_entry.insert(0,4.0)

get_outp()  #get the PSU's output on start

root.after(100, task)   #First Callback

root.mainloop()
